Website of the Accidental Shakespeare Theatre Company
=====================================================

This is the git repository for the website of the Accidental Shakespeare
Theatre Company.

Viewing the Site Online
=======================

If you'd rather not fuss with building the site and only want to view the web
pages on the regular-old internet,
[you can do so here](https://accidentalshakespeare.org). If you find any issues
with the site, feel free to file an issue in this repository!

Getting What You Need to Build the Site
=======================================

If you want to build the site yourself (and maybe even contribute to it?),
you can do that, too.

To build the site, you will need the [hugo](https://gohugo.io) application
which you can get [from github](https://github.com/gohugoio/hugo/releases).
Hint: You'll need one of the "extended" files from the bottom of each list
of releases. You can install hugo by
[following these instructions](https://gohugo.io/getting-started/installing/)

You'll also need the site sources, which you can get from here! To pull-down
the site sources, you will need to use `git`. Once you
[have git installed](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git),
you can run:

`git clone --recursive https://gitlab.com/jwcampbell/accidental-shakespeare.git`

and git will retrieve the site sources for you. You now have everything you
need to build the site on your own computer.

Building the Site for Testing
=============================

From the `accidental-shakespeare` directory, run:

`hugo serve --gc`

in a terminal emulator of your choosing, and it will render the site locally.
You can view the pages at:  [http://localhost:1313](http://localhost:1313). 
Feel free to contact Jim or Benjamin if you have any difficulty problems with
any of the above.

Cheers, and happy viewing!

