+++
eventtype = "show"
type = "productions" # this helps to identify which layout template to use.
title = "Misanthrope"
date = "2012-11-18"
dates_string = "November 18, 2012"
location = "Titzal Café, Chicago"

event_photos = "yes" # Set this to "yes" if we have post-show photos.
  #event_photos should start with the word "perform" (e.g., perform_1.jpg)

playwrights = "Molière"
runtime = "Aproximately one hour and thirty minutes"
intermission_count = "1"

[[production]]
name = "Adrian Balbontin"
title = "Director"

[[cast]]
actor = "Jared McDaris"
role = "Alceste"

[[cast]]
actor = "Ryan Czerwonko"
role = "Oronte"

[[cast]]
actor = "Veronica Blaire"
role = "Basque"

[[cast]]
actor = "Abby Smith"
role = "Celimine"

[[cast]]
actor = "Brandon Jeromy"
role = "Acaste"

[[cast]]
actor = "Eliza Shin"
role = "Arsinoe"

[[cast]]
actor = "Elyse Edelman"
role = "Eliante"

[[cast]]
actor = "George Christophert"
role = "Clitandre"

[[cast]]
actor = "Jacob Louis Grubb"
role = "Philinte"

[[cast]]
actor = "Jeff Kurysz"
role = "DuBois"

[[resources]]
  name = "main"
  src = "main.jpg"

[[resources]]
  name = "perform"
  src = "perform*.jpg"
+++
Before there was political correctness, there was Alceste, whose blunt
truthfulness embarrasses his friends and makes him tremendously unpopular
within respectable society.  But there is the honest woman Eliante who pines for
him, and Célimène who lusts after him, which leads to quirky flirtations,
ripping heartbreaks, and a comedic exile.
