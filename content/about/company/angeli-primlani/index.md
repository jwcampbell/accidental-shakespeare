+++
title = "Angeli Primlani"
pronouns = "She / Her / Hers"
company = "member"
company_title = "Co-Founder, Company Member, Artistic Director Emeritus, EMC"
weight = "14"

[[resources]]
  name = "headshot"
  src = "headshot*"
+++

Angeli Primlani has directed several productions for Accidental Shakespeare,
such as <i>The Tempest</i>, </i>Overruled</i> and <i>Macbeth</i>. Angeli has
worked in regional theaters in the Southeast and in the Czech Republic. She's
also an accomplished writer and a playwright, whose work has been performed in
Chicago by Rasaka and Otherworld Theater Company. She received the 3Arts
Ragdale Fellowship in Theater Arts in 2010. Angeli holds a BA in Theatre and
English from the University of North Carolina at Chapel Hill and a Masters in 
journalism from Northwestern University.

