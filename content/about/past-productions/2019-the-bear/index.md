+++
eventtype = "show"
type = "productions" # this helps to identify which layout template to use.
# The 'date' is not the show date. It should be the date you created the page
date = "2019-03-16T19:30:00"
title = "An Evening of Chekhov"
subtitle = "A performance of 'The Bear' and a scene from 'Three Sisters'"

tickets_available = "no" # If you enter "yes" here, a ticket link will appear!
ticket_call_to_action = "Get Tickets!"
ticket_link = "https://www.brownpapertickets.com/event/4193298"

event_photos = "yes" # Set this to "yes" if we have post-show photos.
  #event_photos should start with the word "perform" (e.g., perform_1.jpg)

playwrights = "Anton Chekhov"
runtime = "Aproximately one hour and thirty minutes"
intermission_count = "1"

[[production]]
name = "Iris Sowlat"
title = "Director, The Bear"

[[production]]
name = "Michelle Altman"
title = "Assistant Director, The Bear"

[[production]]
name = "Eden Blattner"
title = "Director, Three Sisters"

[[cast]]
actor = "Sherry Legare"
role = "Elena Ivanovna Popova (The Bear)"

[[cast]]
actor = "Gary Henderson"
role = "Grigory Stepanovitch Smirnov (The Bear)"

[[cast]]
actor = "Angeli Primlani"
role = "Lyusya (The Bear)"

[[cast]]
actor = "Carol Melnick"
role = "Anfisa (Three Sisters)"

[[cast]]
actor = "Lindsey Zanatta"
role = "Olga (Three Sisters)"

[[cast]]
actor = "Brice Baron"
role = "Natalya (Three Sisters)"

[[cast]]
actor = "Grace DeSant"
role = "Mashsa (Three Sisters)"

[[cast]]
actor = "Jon Vaughn"
role = "Kulygin (Three Sisters)"

[[cast]]
actor = "Quinn Leary"
role = "Vershinin (Three Sisters)"

[[cast]]
actor = "Gary Henderson"
role = "Andrei (Three Sisters)"

# list showtimes in ISO 8601 format with links to tickets
[[showtimes]]
  time = "2019-04-04T19:30:00"
  tickets = "https://jimcampbell.org"

[[venue]]
  name = "The Atlantic Bar and Grill"
  address = "5062 North Lincoln Ave, Chicago, IL 60625"
  map_link = "https://goo.gl/maps/hpfM59eG3v42"

[[resources]]
  name = "main"
  src = "main.jpg"

[[resources]]
  name = "perform"
  src = "perform*.jpg"

+++

#### One Night Only!

Join us for the contrasting sides of Chekhov — tragic and
comic. We'll begin with a scene from Chekhov's play, <i>Three Sisters</i>,
and follow that up with a performance of Chekhov's rarely produced one-act
play, <i>The Bear</i>.

A break between the performances will provide a brief intermission.

#### About The Works

The most recent major production of <i>Three Sisters</i> in New York City was in
2011, by the Classic Stage Company. It was directed by Austin Pendleton and
featured Maggie Gyllenhaal as Masha and Peter Sarsgaard as her lover
Vershinin.

Last performed in Chicago in 1999, the comedic one-act, <i>The Bear</i>, was
described by Chekhov himself as a, “vaudeville in the French style."
