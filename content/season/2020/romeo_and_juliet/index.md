+++
event_type = "performance"
title = "Romeo & Juliet"
details = "yes"
description = """
  In the Spring of 2021, Accidental Shakespeare Company will present its next
  full production, Romeo and Juliet, directed by Company Member Iris Sowlat.
  """
season = "2020"

event_photos = "no" # Set this to "yes" if we have post-show photos.
  #event_photos should start with the word "perform" (e.g., perform_1.jpg)

playwrights = "William Shakespeare, adapted by Iris Sowlat"

tickets_available = "yes"
ticket_link = "https://www.facebook.com/AxShakes"
ticket_call_to_action = "New episodes will launch from Facebook"


[[production]]
title = "Director"
name = "Iris Sowlat"

[[production]]
title = "Film Editor"
name = "Evelyn Landow"

[[production]]
title = "Fight Choreography and Cinematography"
name = "Polley Cooney"

[[production]]
title = "Text Coach"
name = "Julia Kessler"

[[production]]
title = "Stage Manager"
name = "Molly Garrison"

[[production]]
title = "Original Music"
name = "Owen Owens"

[[production]]
title = "Original Graphic Design"
name = "James Anthony"

[[production]]
title = "Queen Mab Speech Video Design and Performance"
name = "Elizabeth Quilter"

[[production]]
title = "Production Manager"
name = "Iris Sowlat"

[[cast]]
actor = "Melody DeRogatis"
role = "Juliet"

[[cast]]
actor = "Sierra Bryn Buffum"
role = "Romeo"

[[cast]]
actor = "Elizabeth Quilter"
role = "Mercutio / Paris"

[[cast]]
actor = "Polley Cooney"
role = "Tybalt / Sampson"

[[cast]]
actor = "AC Rakotoniaina"
role = "Benvolio"

[[cast]]
actor = "Lauren Miller"
role = "The Nurse / The Prince"

[[cast]]
actor = "Julia Kessler"
role = "Friar Lawrence / Gregory"

[[cast]]
actor = "Terri Lynne Hudson"
role = "Lady Capulet"

[[cast]]
actor = "Erik Schiller"
role = "Lord Capulet"

[[cast]]
actor = "Victoria Montalbano"
role = "Lady Montague / Apothecary"

[[cast]]
actor = "Angeli Primlani"
role = "Abram"

+++

Accidental Shakespeare Company presents a virtual film production of Romeo and Juliet, to air in
five weekly installments on their YouTube channel, free of charge, beginning on *May 15, 2021*. 

Links to each episode will be made accessible on Accidental Shakespeare Company’s Facebook page.

This production of Romeo and Juliet was originally announced, cast, and intended to be performed
live, in 2020. Once the COVID-19 pandemic began and all live theatre was postponed, director Iris
Sowlat brought together the original cast and adapted her concept for a digital medium, using Zoom
calls, self-tapes, socially-distant filming, and other video art. 

Iris Sowlat chose to set this production as “...a period piece of the 2020 pandemic. We tell
stories to understand the world around us, and, if we’re producing Romeo and Juliet now, in the
middle of this specific time of national trauma, this current historical moment is the best
possible setting. I’m intentionally using this cultural moment to explore what it’s like to meet
the love of your life, someone who you immediately know really hears you, amidst these uncertain
times, and then be separated from the one you love.” 

The majority of the cast are women and non-binary actors, and Romeo and Juliet are intentionally
played as a queer couple. Sowlat, who has a passion for queering the canon, says that “Romeo and
Juliet is the most beautiful and iconic love story of all time, and I would love to give the world
a chance to see this story unfold with a queer pairing. There’s something so revolutionary about
queer love that is on-mission with the theme of the play. In a world that prioritizes violence,
prioritizing peace, love, and pleasure is a revolutionary act.”  


Romeo and Juliet Introduction
-----------------------------

{{< youtube id="HPb82LDDqgo" title="Romeo and Juliet Introduction" >}}

Why Set Romeo and Juliet in the Pandemic?
-----------------------------------------

{{< youtube id="MvkTVRCbR5A" title="Why set Romeo and Juliet int he Pandemic?" >}}

Why Zoom Theater?
-----------------

{{< youtube id="9pa0cH3mAos" title="Why Zoom Theater?" >}}

