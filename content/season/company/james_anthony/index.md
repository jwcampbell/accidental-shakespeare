+++
title = "James Anthony"
pronouns = "They / Them / Their"
company = "member"
company_title = "Artistic Director"
email = "james@accidentalshakespeare.org"
weight = "10"

[[resources]]
  name = "headshot"
  src = "headshot*"
+++

James Anthony was born and raised in a small town in South Carolina and moved
to Chicago on a Megabus from New Orleans. They have a B.S. in Mass
Communications and Theatre, and M.A in Speech/Theatre. JA is a graduate of the
Entrepreneur Training Program through Operation Hope, and they're a member of
Actors' Equity Association and the Zumba Instructor Network.
 
James Anthony began performing at the age of 5 and has been in the performing
arts for over 25 years. From acting to stage managing, to sound and props
design, and to box office to marketing, they've covered a lot of bases.
However, they realized that there was an issue in the arts, specifically around
arts accessibility, inclusion, and diversity. So, they decided to take a break
from creating art to create change and equity because there are too many
privileged straight/gay white cis men (and women) running the business. 

They're the Director of Marketing and Communications at Chicago Children's
Theatre. They've done audience services, marketing and sales for various
organizations including About Face Theatre, Actors Gymnasium Circus School and
Theatre Company, Chicago Fringe Festival, Evanston Chamber of Commerce, Pride
Films and Plays, and Nothing Without A Company. 

To learn more about James Anthony, be on the lookout for their website
sissyearl.com launching soon. 
