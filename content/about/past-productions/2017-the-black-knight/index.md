+++
eventtype = "show"
type = "productions" # this helps to identify which layout template to use.
date = "2017-02-19"
title = "The Black Knight"
subtitle = "The world premiere of The Black Knight, an original work by the Accidental Shakespeare Theater Company's Artistic Director, Angeli Primlani."

tickets_available = "no" # If you enter "yes" here, a ticket link will appear!
ticket_call_to_action = "Get Tickets!"
ticket_link = "https://www.brownpapertickets.com/event/4193298"

event_photos = "yes" # Set this to "yes" if we have post-show photos.
  #event_photos should start with the word "perform" (e.g., perform_1.jpg)

playwrights = "Angeli Primlani"
runtime = "Approximately one hour and thirty minutes"
intermission_count="0"
location = "Stage 773, Chicago"

[[production]]
name = "Angeli Primlani"
title = "Director"

[[production]]
name = "Stacey Hanlon"
title = "Sound Design"

[[production]]
name = "Catherine Cefalu"
title = "Props Design"

[[production]]
name = "Benjamin Dionysus"
title = "Lighting Design"

[[cast]]
actor = "Chris Aruffo"
role = "Radio Announcer"

[[cast]]
actor = "Taylor Galloway"
role = "Henza, and others"

[[cast]]
actor = "Gary Henderson"
role = "Albrecht"

[[cast]]
actor = "Robert Kaercher"
role = "Forrester"

[[cast]]
actor = "Julia Kessler"
role = "Kathi"

[[resources]]
  name = "main"
  src = "main.jpg"

[[resources]]
  name = "perform"
  src = "perform*.jpg"

+++

A love story between a Jewish spy and SS Captain of the Intelligence Division set in Prague during the height of the Nazi occupation during World War II. This world premiere play by Angeli Primlani was performed as a staged reading.
