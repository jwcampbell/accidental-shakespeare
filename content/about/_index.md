+++
title = "About"
type = "about"
description = "About the Accidental Shakespeare Company"
draft = false
history = """
   Accidental Shakespeare was formed in the Spring of 2011 by a group of
   actors, designers and writers who share a love of heightened language and
   wit. The company became a formal 501(c)3 organization in 2016.
   """

mission = """
   Accidental Shakespeare is an ensemble theatre company that thoughtfully
   explores challenging plays, to forge connections between classical themes
   and contemporary culture in intimate settings.
   """

[[resources]]
  name = "rehearsal"
  src = "rehearsal.jpg"

+++
