+++
eventtype = "show"
type = "productions" # this helps to identify which layout template to use.
date = 2011-09-25T00:00:00.000Z
title = "Much Ado About Nothing"
subtitle = "A Staged Reading"

tickets_available = "no" # If you enter "yes" here, a ticket link will appear!
ticket_call_to_action = "Get Tickets!"
ticket_link = "https://www.acccidentalshakespeare.org"

event_photos = "yes" # Set this to "yes" if we have post-show photos.
  #event_photos should start with the word "perform" (e.g., perform_1.jpg)

[[production]]
name = "Margaretta Sacco"
title = "Director"

[[cast]]
actor = "Laura Sturm"
role = "Beatrice"

[[cast]]
actor = "Ryan Swinkle"
role = "Benedict"

[[cast]]
actor = "Robin Billadeau"
role = "Claudio"

[[cast]]
actor = "Christa Sablic"
role = "Hero"

[[cast]]
actor = "Benjamin Dionysus"
role = "Borachio/Friar Francis"

[[cast]]
actor = "Angeli Primlani"
role = "Balthasar/Messenger/Margaret"

[[cast]]
actor = "John Amedio"
role = "Leonardo/Conrade"

[[cast]]
actor = "Jamie Black"
role = "Verges"

[[cast]]
actor = "Philip Meyer"
role = "Don John"

[[cast]]
actor = "Julia Kessler"
role = "Don Pedro/Sexton"

[[cast]]
actor = "Eliza Hoffman"
role = "Ursula"

[[resources]]
  name = "main"
  src = "main.jpg"

[[resources]]
  name = "perform"
  src = "perform*.jpg"
+++
Benedict and Beatrice hate each other, or are they secretly in love? It’s a
battle of the sexes comedy set in Messina, Italy, that’s Mardi Gras, carnival
festival themed.
