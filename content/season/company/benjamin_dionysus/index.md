+++
title = "Benjamin Dionysus"
pronouns = "He / Him / His"
company = "member"
company_title = "Co-Founder and Company Member"
weight = "12"

[[resources]]
  name = "headshot"
  src = "headshot.jpg"
+++

Benjamin Dionysus is a lighting designer and mask maker for Accidental
Shakespeare. In Chicago he also regularly designs for We Three, Clock Theatre,
and EDGE, and when Benjamin lived in NYC, he worked for the Gertrude Stein
Repertory Theatre. He studied lighting design at Smith College, with Nancy
Schertler and Heather Carson. His online portfolio of selected work is
available at [sixbynine.com](http://www.sixbynine.com).
