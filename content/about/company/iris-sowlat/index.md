+++
title = "Iris Sowlat"
pronouns = "She / Her / Hers"
company = "member"
company_title = "Company Member"
weight = "15"

[[resources]]
  name = "headshot"
  src = "headshot*"
+++

Iris Sowlat is thrilled to join Accidental Shakespeare Company and to direct
<i>R & J</i> next summer! Previously with Accidental Shakespeare, Iris directed
Checkhov's <i>The Bear</i>. Other directing credits include ABCD
(Piven Theatre's Lab), The Days Are Shorter (Pride Films & Plays), Joan of Arc
and Underworld Anthem (both RhinoFest), and Narratives of Achromatopsia and
Turn That Thing Around (both Chicago Fringe Festival). Assistant-directing
credits include Into the Breeches! (Northlight) and work with About Face,
Broken Nose, and Fury Theatre. She has directed readings, workshops, and short
plays with American Blues Theatre, Collaboraction, Otherworld, the Chicago
Theatre Marathon, the Women's Theatre Alliance, Global Hive Labs, NoPassport
Theatre Alliance, the Chicago Women's History Center, and more. Iris is a proud
recipient of Windy City Times' "30 Under 30." You can learn more about Iris on
her website, [IrisSowlat.com](https://IrisSowlat.com).
