+++
event_type = "performance"
title = "Star Crossed Lovers Fest"
details = "no"  #skipping the details because this show doesn't have a typical cast, director, etc.
description = """
  Inspired by the themes of Romeo and Juliet, Accidental Shakespeare Company presents their first
  Zoom theatre festival!
  """
season = "2020"

event_photos = "no" # Set this to "yes" if we have post-show photos.
  #event_photos should start with the word "perform" (e.g., perform_1.jpg)
+++

Inspired by the themes of Romeo and Juliet, Accidental Shakespeare Company presents their first
Zoom theatre festival!

This evening of digital theatre includes short plays, stories, and performance art that explore
the theme of “star-crossed love,” being separated from the one you love, missed connections,
breakups, and more. 

**Performance Details:**

Saturday, September 19, at 7:30 PM CST

Live-Streamed to Accidental Shakespeare Company’s **[Facebook page](https://www.facebook.com/AxShakes)**. 

**Performances include**:

    • “Funny How That Works” by Hannah Chamberlin, directed by Iris Sowlat
    • “Dick” - personal storytelling by Victoria Montalbano 
    • The dance piece “Relationship” by Jess Martin
    • "Plan B" by Maria Welser, directed by Justin Sacramone 
    • A performance art piece by Fay Florence-Steddum 
    • “New Bedford” by Hope Campbell, directed by Melody DeRogatis 
    • The song "Romeo and Juliet" by Mark Knopfler, sung by Angeli Primlani
    • “They Came Too Late” by Rachel Weekley, directed by Ashley Lauren Rodgers
    • “Don’t Go” by Valerie deGroot, directed by Iris Sowlat
    • “Don’t Get Your Facts Straight” by Allison Fradkin, directed by Iris Sowlat

