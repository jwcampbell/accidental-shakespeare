+++
title = "In Memoriam"
type = "in-memoriam"
subtitle = "David Denman"
weight = 20
draft = false
+++

We mourn the sudden death of our longtime Board member David Denman. In his
board service to Accidental Shakespeare and Kidworks, as well as his
playwriting and set-design work with Clock Productions, he exemplified
commitment to the arts.

We will miss his positivity and kindness and are so grateful to have known him.
