+++
eventtype = "show"
type = "productions" # this helps to identify which layout template to use.
title = "Twelfth Night"
date = "2012-06-22"
location = "Act One Studios - Chicago"

playwrights = "William Shakespeare"

[[production]]
name = "Laura Sturm"
title = "Director"

[[production]]
name = "David Hathway"
title = "Assistant Director/Musical Director"

[[production]]
name = "Angeli Primlani"
title = "Stage Manager"

[[production]]
name = "Mike Evans, Ryan Swikle, Angeli Primlani, David Hathway"
title = "Musicians"

[[production]]
name = "Gary Henderson"
title = "Fight Choreographer"

[[production]]
name = "Jim Campbell"
title = "Front of House"

[[production]]
name = "Julia Kessler/Sherry Legare"
title = "Catering"

[[production]]
name = "Ben Aldred/Robin Billadeau"
title = "Marketing"

[[cast]]
actor = "Julia Kessler"
role = "Andrew Aguecheek"
actor_bio_url = "/company/julia_kessler"

[[cast]]
actor = "Katie Suffern"
role = "Viola"

[[cast]]
actor = "Kelly Lynn Hogan"
role = "Maria"

[[cast]]
actor = "Laura Sturm"
role = "Olivia"

[[cast]]
actor = "Ryan Swikle"
role = "Toby"

[[cast]]
actor = "David Hathway"
role = "Feste"

[[cast]]
actor = "David Fehr"
role = "Orsino"

[[cast]]
actor = "Eric Casady"
role = "Sebastian/Valentine"

[[cast]]
actor = "Geoff Zimmerman"
role = "Captain/Priest/Officer"

[[cast]]
actor = "Anne Thompson"
role = "Malvolio"

[[cast]]
actor = "Gary Henderson"
role = "Antonio"

[[cast]]
actor = "Sherry Legare"
role = "Fabian"
actor_bio_url = "/company/sherry_legare"

[[resources]]
  name = "main"
  src = "main.jpg"

[[resources]]
  name = "perform"
  src = "perform*.jpg"
+++
Twins Viola and Sebastian are separated at shipwreck and land on Illyria.
Viola disguises herself as a boy and falls into a love triangle among Duke
Orsino and Olivia. Sebastian is rescued by Antonio who loves him fiercely.
This comedy has drunken revelries and mistaken identities set during the rock
and roll 1950s.
