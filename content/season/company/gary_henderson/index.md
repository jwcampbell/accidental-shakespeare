+++
title = "Gary Henderson"
pronouns = "He / Him / His"
company = "member"
company_title = "Company Member"
weight = "14"
+++

Chicago credits include Poe in Nightfall with Edgar Allan Poe (Theatre Hikes),
Clayton in Hound of the Baskervilles (Idle Muse Theatre), Seth in The Boys
Upstairs (Pride Films and Plays), Montresor in The Madness of Edgar Allen Poe
(First Folio), Octavius in Julius Caesar (Muse of Fire), and a Townsperson in
Enemy of the People (Goodman Theatre). Regional credits include Oswald in
King Lear and understudy for Demetrius in A Midsummer Night's Dream (Great
River Shakespeare).
