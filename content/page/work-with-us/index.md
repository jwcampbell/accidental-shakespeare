+++
title = "Work With Us"
type = "work-with-us"
description = "Work with Accidental Shakespeare"
welcome = "We're glad you're here!"
weight = 20
draft = false

aliases = [
    "/join",
    "/join.html"
]
+++

Whether you're interested in working with Accidental Shakespeare as a
performer, or want to work with us in another capacity, we welcome both new
contributors and submissions at any time throughout the season.

If you'd like to join us in creating future performances, contact us at
[accshakes@gmail.com](mailto:accshakes@gmail.com).

When needed, we will also post company audition and job openings to the
[Theatre in Chicago](https://www.theatreinchicago.com/) and
[League of Chicago Theatres](https://leagueofchicagotheatres.org/) websites.
