+++
eventtype = "show"
type = "productions" # this helps to identify which layout template to use.
title = "Tartuffe"
subtitle = "A staged reading"
date = "2014-06-08"
location = "Chief O’Neill’s Pub & Restaurant, Chicago"

playwrights = "Molière"

tickets_available = "no" # If you enter "yes" here, a ticket link will appear!
event_photos = "no"

[[production]]
name = "Margaretta Sacco"
title = "Director"

[[cast]]
actor = "Ben Muller"
role = "Tartuffe"

[[cast]]
actor = "Rocco Renda"
role = "Orgon"

[[cast]]
actor = "Christian Isely"
role = "Cleante"

[[cast]]
actor = "Geoff Zimmerman"
role = "Damic/Officer"

[[cast]]
actor = "Kevin Sheehan"
role = "Valere"

[[cast]]
actor = "Anu Bhatt"
role = "Elmire"

[[cast]]
actor = "Julia Kessler"
role = "Dorine"
actor_bio_url = "/company/julia_kessler"

[[cast]]
actor = "Christie Coran"
role = "Mariane/Monsieur"

[[cast]]
actor = "Elizabeth Rentfro"
role = "Madame Pernelle"

[[resources]]
  name = "main"
  src = "main.jpg"

+++

Accidental Shakespeare Theatre Company performs a staged reading of one of
Moliere's best loved -- and most clever -- comedies. Few scoundrels are as
roguish, charming or hilarious as the titular Tartuffe, an impostor who
insinuates himself into the home of Orgon and his family, quickly throwing the
entire household into chaos. As Orgon's infatuation with his guest and his
phony piety grows, Tartuffe's deception threatens to turn downright damaging.
