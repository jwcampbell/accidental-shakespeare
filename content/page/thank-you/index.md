+++
title = "Thank You"
type = "thank-you"
description = "Thank you for your donation!"
weight = 40
draft = false

aliases = [
    "/thankyou",
    "/thankyou.html",
    "/thank_you.html"
]
+++

We can no other answer make, but, thanks, and thanks!
