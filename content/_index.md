+++
title = "The Accidental Shakespeare Theatre Company"
description = """
  Accidental Shakespeare is an ensemble theatre company that thoughtfully
  explores challenging plays, to forge connections between classical themes
  and contemporary culture in intimate settings.
  """
sort_by = "none"
weight = 0
type = "index"
insert_anchor_links = "none"
draft = false

[[resources]]
  src = "*.png"
  
+++

