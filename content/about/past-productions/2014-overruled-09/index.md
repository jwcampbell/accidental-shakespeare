+++
eventtype = "show"
type = "productions" # this helps to identify which layout template to use.
title = "Overruled"
date = "2014-09-21"
location = "Chief O’Neill’s Pub & Restaurant, Chicago"
playwrights = "George Bernard Shaw"
event_photos = "yes" # Set this to "yes" if we have post-show photos.
  #event_photos should start with the word "perform" (e.g., perform_1.jpg)

[[production]]
name = "Angeli Primlani"
title = "Director"

[[production]]
name = "Benjamin Dionysus"
title = "Stage Manager/Lighting Design"

[[production]]
name = "Brigid Duffy and Frank Mahon of Stone Hearth Theatre"
title = "Co-producers"

[[cast]]
actor = "Sherry Legare"
role = "Mrs. Lunn"

[[cast]]
actor = "Laurie Lister"
role = "Mrs. Juno"

[[cast]]
actor = "Chris Aruffo"
role = "Mr. Juno"

[[cast]]
actor = "Gary Henderson"
role = "Mr. Lunn"

[[resources]]
  name = "main"
  src = "main.jpg"

[[resources]]
  name = "perform"
  src = "perform*.jpg"

+++
George Bernard Shaw's delightful one-act play Overruled (1913) finds the Irish dramatist at his most entertaining and fun. Two married couples take a break from their spouses, leading to an examination of fidelity and adultery. Though not nearly as famous as some of Shaw's other work, such as Pygmalion, and lacking the heavy messages of the acclaimed playwright's weightier works, like Arms and the Man, Overruled offers the chance to laugh at love, lovers and life itself.
