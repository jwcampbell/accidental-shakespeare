These are the images for the homepage!

# Carousel Images

- All of the files starting with 'carousel' and ending in '.jpg' will be
  included in the carousel.
- They'll be included in "alphabetical" order. Although we've started by
  listing them as carousel-01.jpg, carousel-02.jpg, etc., they can be named
  whatever we want (e.g., carousel-sherry.jpg).

# Other Images

- More info here.
