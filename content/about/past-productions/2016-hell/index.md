+++
eventtype = "show"
type = "productions" # this helps to identify which layout template to use.
title = "Hell"
date = 2016-10-08T00:00:00.000Z
subtitle = "A staged reading of Hell."
location = "McKaw Theater, Chicago"

tickets_available = "no"
event_photos = "yes"

playwrights = "Upton Sinclair"
runtime = "Aproximately one hour and thirty minutes"
intermission_count = "1"

[[production]]
name = "Chris Aruffo"
title = "Director"

[[production]]
name = "Tiffany Tuck"
title = "Stage Manager"

[[production]]
name = "Benjamin Dionysus"
title = "Lighting Design/Projections"

[[cast]]
actor = "Jake Degler"
role = "First Imp/Attendant/Stagehand/Harry/Mike/The Real Devil"

[[cast]]
actor = "Kaelea Rovinsky"
role = "Second Imp/Whit o’Wit/Comrade Jesus"

[[cast]]
actor = "Linsey Summers"
role = "Beelzebub/Justice/Wobbly/John"

[[cast]]
actor = "Taylor Galloway"
role = "Belial/Angel of Humor/Dick"

[[cast]]
actor = "Taylor Barton"
role = "Moloch/Tom/Pete/Author"

[[cast]]
actor = "Julia Kessler"
role = "Astarte/Mother/Jim/Karl"

[[cast]]
actor = "Jared McDaris"
role = "Mammon/Bill Haywood"

[[cast]]
actor = "Christopher Sylvie"
role = "Satan/Lieutenant/Budge/Joe"

[[cast]]
actor = "Heather Branham Green"
role = "Attorney-General/Angel of Love/Police Sergeant/Bill"

[[resources]]
  name = "main"
  src = "main.jpg"

[[resources]]
  name = "perform"
  src = "perform*.jpg"
+++

Does it sometimes feel like the capitalist system is ... demonic? Well, there
might just be a reason for that. Upton Sinclair, better known as the author of
the muckraking novel The Jungle created this acidic, satiric tale of demons,
angels, pacifists, Wobblies, and the poor schmucks who fought World War I.
It's not your imagination. This just might be Hell on Earth. Although his
classic novel The Jungle is credited with the creation of the Pure Food and
Drug Act, he was dissatisfied with only reforming the meat packing industry.
Saying, \"'I aimed for the public's heart, and... hit it in the stomach\".
Sinclair tried again in 1924, this time focusing on Western Capitalism as a
whole, and militarism specifically.
