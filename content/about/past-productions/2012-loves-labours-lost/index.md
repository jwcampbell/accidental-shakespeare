+++
eventtype = "show"
type = "productions" # this helps to identify which layout template to use.
date = "2011-01-29"
title = "Loves Labour's Lost"
subtitle = "A Staged Reading"
location = "Titzal Café"

tickets_available = "no" # If you enter "yes" here, a ticket link will appear!
ticket_call_to_action = "Get Tickets!"
ticket_link = "https://accidentalshakespeare.org"

[[production]]
name = "Ben Aldred"
title = "Director"

[[cast]]
actor = "Julia Kessler"
role = "Princess"
actor_bio_url = "/company/julia_kessler"

[[cast]]
actor = "Jared McDaris"
role = "Don Armando"

[[cast]]
actor = "Kelly Lynn Hogan"
role = "Maria"

[[cast]]
actor = "Kate Suffern"
role = "Rosaline"

[[cast]]
actor = "Kristie Forsch"
role = "Jacquenetta"

[[cast]]
actor = "Lane Flores"
role = "Moth/Boyet"

[[cast]]
actor = "Richard Alpert"
role = "Holofernes"

[[cast]]
actor = "Robin Billadeau"
role = "George Seacoa"

[[cast]]
actor = "Eli Branson"
role = "Berown"

[[cast]]
actor = "Gary Henderson"
role = "Longaville"

[[cast]]
actor = "Greg Pragel"
role = "Costard"

[[resources]]
  name = "main"
  src = "main.jpg"

[[resources]]
  name = "perform"
  src = "perform*.jpg"
+++

The King of Navarre and three of his companions vow to avoid the company of women in order to pursue intense studies only to be interrupted by the Princess of France and her lovely companions.  A comedy ensues in this modern hipster urban environment.
