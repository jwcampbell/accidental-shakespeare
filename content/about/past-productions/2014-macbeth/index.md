+++
eventtype = "show"
type = "productions" # this helps to identify which layout template to use.
title = "Macbeth"
date = "2014-10-08"
dates_string = "October 8 - November 2, 2014"
location = "McKaw Theater, Chicago"

event_photos = "yes" # Set this to "yes" if we have post-show photos.
  #event_photos should start with the word "perform" (e.g., perform_1.jpg)

playwrights = "William Shakespeare"
runtime = "Aproximately one hour and thirty minutes"
intermission_count = "1"

[[production]]
name = "Angeli Primlani"
title = "Director"

[[production]]
name = "Sherry Legare"
title = "Producer"
bio_url = "/company/sherry_legare"

[[production]]
name = "Cat Cefalu"
title = "Stage Manager"
bio_url = "/company/cat_cefalu"

[[production]]
name = "Caitlin Aruffo"
title = "Sound Designer"

[[production]]
name = "Leala Avdich"
title = "Marketing/PR"

[[production]]
name = "Kate Setzer Kamphausen"
title = "Costume Design"

[[production]]
name = "Buckman Page"
title = "Original Song Contributions"

[[production]]
name = "H. Russ Brown"
title = "Violence Design"

[[production]]
name = "Benjamin Dionysus"
title = "Lighting Design"
bio_url = "/company/benjamin_dionysus"

[[production]]
name = "Liz Homsy"
title = "Technical Director/Production Assistant"

[[production]]
name = "Margaretta Sacco"
title = "Properties Design"

[[cast]]
actor = "Mary-Kate Arnold"
role = "Witch"

[[cast]]
actor = "Brian Bradford"
role = "Malcolm"

[[cast]]
actor = "Christie Coran"
role = "Witch"

[[cast]]
actor = "Amy Gorelow"
role = "Witch"

[[cast]]
actor = "Evan Johnson"
role = "Banquo/Seyton"

[[cast]]
actor = "Julia Kessler"
role = "Lady Macduff/Gentlewoman/Young Siward"
actor_bio_url = "/company/julia_kessler"

[[cast]]
actor = "Sherry Legare"
role = "Lady Macbeth"
actor_bio_url = "/company/sherry_legare"

[[cast]]
actor = "Jared McDaris"
role = "Duncan/Porter/Old Man/Doctor"

[[cast]]
actor = "Andrew Mehegan"
role = "Macbeth"

[[cast]]
actor = "Rocco Renda"
role = "Ross"

[[cast]]
actor = "Julia Rigby"
role = "Fleance/Young Macduff/Siward"

[[cast]]
actor = "Raker Wilson"
role = "Macduff"

[[resources]]
  name = "main"
  src = "main.jpg"

[[resources]]
  name = "perform"
  src = "perform*.jpg"
+++
Drawing on her experience as a reporter, and her stint writing for a regional newspaper in D.C., director Angeli Primlani places the Scottish Play into the anxious period of the first Bush Administration. The play is about ambition, and the lengths one man will go to justify himself. Fight designer H. Russ Brown brings his gleeful passion for mayhem to the fight choreography. Jeff-award winning costume designer Kate Setzer Kamphausen puts the cast into the particular dress code of the Bush-era political culture. Local Indie-Rock band Buckman Page contributes their lyric, hopeful music to the show.
