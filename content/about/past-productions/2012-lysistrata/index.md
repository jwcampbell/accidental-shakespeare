+++
eventtype = "show"
type = "productions" # this helps to identify which layout template to use.
title = "Lysistrata"
subtitle = "A Staged Reading"
date = "2012-09-09"
location = "Titzal Café, Chicago"

tickets_available = "no" # If you enter "yes" here, a ticket link will appear!
ticket_call_to_action = "Get Tickets!"
ticket_link = "https://www.brownpapertickets.com/event/4193298"

playwrights = "Aristophanes"

event_photos = "no" # Set this to "yes" if we have post-show photos.
  #event_photos should start with the word "perform" (e.g., perform_1.jpg)

[[production]]
name = "Margaretta Sacco"
title = "Director"

[[cast]]
actor = "Angeli Primlani"
role = "Lysistrata"

[[cast]]
actor = "Michael Rashid"
role = "Kinesias"

[[cast]]
actor = "Regina Leslie"
role = "Myrrhine"

[[cast]]
actor = "Anneliese Moffitt"
role = "Lampito/Chorus"

[[cast]]
actor = "Sherry Legare"
role = "Theban Woman/Defector/Door Keeper/Chorus"
actor_bio_url = "/company/sherry_legare"

[[cast]]
actor = "Julia Kessler"
role = "Kalonike"
actor_bio_url = "/company/julia_kessler"

[[cast]]
actor = "Jared McDaris"
role = "Spartan Ambassador/Messenger/Chorus"

[[cast]]
actor = "John Amedio"
role = "Athenian Commissioner/Chorus"

[[resources]]
  name = "main"
  src = "main.jpg"
+++

Ever wonder what would happen if women refused to have sex with their men until they stopped going to war? Well, so did the ancient Greeks.
