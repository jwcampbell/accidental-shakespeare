+++
eventtype = "show"
type = "productions" # this helps to identify which layout template to use.
title = "Boston Marriage"
subtitle = "A performance of David Mamet's Boston Marriage"
date = "2016-02-11"
location = "Berger Park Coach House, Chicago"

tickets_available = "no"
event_photos = "yes"

playwrights = "David Mamet"
runtime = "Aproximately one hour and fiteen minutes"
intermission_count = "1"

[[production]]
name = "Angeli Primlani"
title = "Director"

[[production]]
name = "Sherry Legare"
title = "Producer"

[[production]]
name = "Matt Cefalu"
title = "Stage Manager/Sound and Properties Design"

[[production]]
name = "Benjamin Dionysus"
title = "Lighting Design"

[[production]]
name = "Kate Setzer Kamphausen"
title = "Costume Design"

[[production]]
name = "David Denman"
title = "Set Design"

[[cast]]
actor = "Sherry Legare"
role = "Anna"

[[cast]]
actor = "Julia Kessler"
role = "Claire"

[[cast]]
actor = "Mary-Ann Arnold"
role = "Catherine"

[[resources]]
  name = "main"
  src = "main.jpg"

[[resources]]
  name = "perform"
  src = "perform_*.jpg"
+++

Anna and Claire have been lovers for years, when Anna becomes the mistress of
a wealthy man in order to support them both. But of course it is nowhere near
that simple. What does fidelity mean when one's romantic partnership can
barely be imagined as real? Does money distort women's sexual and personal
integrity? Is chintz a sign of love or a form of psychological torture? Must
we always hurt the ones we love? And what about ... the maid?
