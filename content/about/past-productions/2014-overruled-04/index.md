+++
eventtype = "show"
type = "productions" # this helps to identify which layout template to use.
title = "Overruled"
date = "2014-04-11"
location = "Mary's Attic, Chicago"
playwrights = "George Bernard Shaw"

event_photos = "yes" # Set this to "yes" if we have post-show photos.
  #event_photos should start with the word "perform" (e.g., perform_1.jpg)

[[production]]
name = "Angeli Primlani"
title = "Director"

[[production]]
name = "Benjamin Dionysus"
title = "Stage Manager/Lighting Design"

[[cast]]
actor = "Sherry Legare"
role = "Mrs. Lunn"

[[cast]]
actor = "Laurie Lister"
role = "Mrs. Juno"

[[cast]]
actor = "Chris Aruffo"
role = "Mr. Juno"

[[cast]]
actor = "Gary Henderson"
role = "Mr. Lunn"

[[resources]]
  name = "main"
  src = "main.jpg"

[[resources]]
  name = "perform"
  src = "perform*.jpg"
+++
Two couples taking a holiday from their respective spouses discover that they
have accidentally swapped spouses in their adulterous affairs!  Overruled is a
funny exploration of the world of marriage.
