+++
eventtype = "show"
type = "productions" # this helps to identify which layout template to use.
title = "The Tempest"
date = "2013-02-16"
location = "The Heartland Studio, Chicago"

event_photos = "yes" # Set this to "yes" if we have post-show photos.
  #event_photos should start with the word "perform" (e.g., perform_1.jpg)

playwrights = "William Shakespeare"
runtime = "Aproximately one hour and thirty minutes"
intermission_count = "1"

[[production]]
name = "Sherry Legare"
title = "Producer"

[[production]]
name = "Angeli Primlani"
title = "Director"

[[production]]
name = "Adrian Balbontin"
title = "Assistant Director"

[[production]]
name = "Cat Cefalu"
title = "Stage Manager"

[[production]]
name = "Patrick Ham"
title = "Scenic Design"

[[production]]
name = "Kate Setzer Kamphausen"
title = "Costume Design"

[[production]]
name = "Margaretta Sacco"
title = "Properties Design"

[[production]]
name = "Benjamin Dionysus"
title = "Lighting Design"

[[production]]
name = "Ashly Dalene"
title = "Movement Design"

[[production]]
name = "Melissa Schlesinger"
title = "Sound Design/Score"

[[production]]
name = "Patrick Van Howe"
title = "Scenic Artist"

[[production]]
name = "John Amedio"
title = "Marketing"

[[production]]
name = "Jimmy Neenan"
title = "Set Crew"

[[cast]]
actor = "John Amedio"
role = "Antonio"

[[cast]]
actor = "Mary-Kate Arnold"
role = "Miranda"

[[cast]]
actor = "Christopher Aruffo"
role = "Prospero"

[[cast]]
actor = "Chris Berghoff"
role = "Ferdinand"

[[cast]]
actor = "Jamel Booth"
role = "Caliban"

[[cast]]
actor = "Gary Henderson"
role = "Trinculo"

[[cast]]
actor = "Evan Johnson"
role = "Sebastian"

[[cast]]
actor = "Julia Kessler"
role = "Gonzalo"

[[cast]]
actor = "Jared McDaris"
role = "Ariel"

[[cast]]
actor = "Andrew Mehegan"
role = "Stefano"

[[cast]]
actor = "Aaron Wertheim"
role = "Alonso"

[[resources]]
  name = "main"
  src = "main.jpg"

[[resources]]
  name = "perform"
  src = "perform*.jpg"
+++
The Tempest is often thought of as a show about endings. It is one of
Shakespeare’s last plays and it does deal with mortality and loss of power.
But it is also a show about beginnings, the kind that you only get by shucking
off the burden of the past and finding freedom in the future. This Alchemy
Punk version came about after the founders of Accidental Shakespeare
took in a showing of Julie Taymor’s film version of the play.
