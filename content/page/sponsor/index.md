+++
title = "sponsor"
description = "Sponsors of The Accidental Shakespeare Theatre Company"
sort_by = "none"
weight = 0
type = "sponsor"
draft = false

[[resources]]
  name = "sponsor"
  src = "images/about_face_theatre.jpg"
    [resources.params]
    title = "About Face Theatre"
    website = "http://aboutfacetheatre.com/"

[[resources]]
  name = "sponsor"
  src = "images/actors_gymnasium.jpg"
    [resources.params]
    title = "The Actor's Gymnasium"
    website = "https://www.actorsgymnasium.org/"

[[resources]]
  name = "sponsor"
  src = "images/big_shoulders_coffee.jpg"
    [resources.params]
    title = "Big Shoulders Coffee"
    website = "https://bigshoulderscoffee.com"

[[resources]]
  name = "sponsor"
  src = "images/chicago_bears.jpg"
    [resources.params]
    title = "Chicago Bears"
    website = "https://chicagobears.com"

[[resources]]
  name = "sponsor"
  src = "images/chicago_opera_theater.jpg"
    [resources.params]
    title = "Chicago Opera Theater"
    website = "https://www.chicagooperatheater.org"

[[resources]]
  name = "sponsor"
  src = "images/chicago_white_sox.jpg"
    [resources.params]
    title = "Chicago White Sox"
    website = "https://www.mlb.com/whitesox"

[[resources]]
  name = "sponsor"
  src = "images/city_lit_theater.jpg"
    [resources.params]
    title = "City Lit Theater"
    website = "https://www.citylit.org"

[[resources]]
  name = "sponsor"
  src = "images/davis_carbon_arc.jpg"
    [resources.params]
    title = "Davis Theater Carbon Arc Bar"
    website = "https://www.davistheater.com/carbon-arc-bar/"

[[resources]]
  name = "sponsor"
  src = "images/edzos_burger_shop.jpg"
    [resources.params]
    title = "Edzo's Burger Shop"
    website = "http://edzos.com/"

[[resources]]
  name = "sponsor"
  src = "images/emerald_city_theater.jpg"
    [resources.params]
    title = "Emerald City Theater"
    website = "http://www.emeraldcitytheatre.com"

[[resources]]
  name = "sponsor"
  src = "images/fleet_feet_sports.jpg"
    [resources.params]
    title = "Fleet Feet Sports"
    website = "http://www.fleetfeetchicago.com"

[[resources]]
  name = "sponsor"
  src = "images/halcyon_theatre.jpg"
    [resources.params]
    title = "Halcyon Theatre"
    website = "https://halcyontheatre.org"

[[resources]]
  name = "sponsor"
  src = "images/hoosier_mama.jpg"
    [resources.params]
    title = "Hoosier Mama"
    website = "https://www.hoosiermamapie.com"

[[resources]]
  name = "sponsor"
  src = "images/house_theatre.jpg"
    [resources.params]
    title = "The House Theatre"
    website = "https://www.thehousetheatre.com/"

[[resources]]
  name = "sponsor"
  src = "images/jenis.jpg"
    [resources.params]
    title = "Jeni's Splendid Ice Creams"
    website = "https://jenis.com/"

[[resources]]
  name = "sponsor"
  src = "images/io_theater.jpg"
    [resources.params]
    title = "IO Theater"
    website = "https://www.ioimprov.com"

[[resources]]
  name = "sponsor"
  src = "images/kokandy_productions.jpg"
    [resources.params]
    title = "Kokandy Productions"
    website = "http://www.kokandyproductions.com"

[[resources]]
  name = "sponsor"
  src = "images/leonidas_chocolates.jpg"
    [resources.params]
    title = "Leonidas Chocolates"
    website = "https://leonidas-chocolate.com"

[[resources]]
  name = "sponsor"
  src = "images/lickity_split.jpg"
    [resources.params]
    title = "Lickity Split"
    website = "https://www.lickitysplitchicago.com"

[[resources]]
  name = "sponsor"
  src = "images/metropolis_coffee_company.jpg"
    [resources.params]
    title = "Metropolis Coffee"
    website = "https://www.metropoliscoffee.com"

[[resources]]
  name = "sponsor"
  src = "images/mrs_murphys.jpg"
    [resources.params]
    title = "Mrs Murphy's Irish Bistro"
    website = "https://irishbistro.com/"

[[resources]]
  name = "sponsor"
  src = "images/promethian_theatre_ensemble.jpg"
    [resources.params]
    title = "Promethian Theatre"
    website = "https://www.prometheantheatre.org"

[[resources]]
  name = "sponsor"
  src = "images/stage_left_theatre.jpg"
    [resources.params]
    title = "Stage Left Theatre"
    website = "https://www.stagelefttheatre.com"

[[resources]]
  name = "sponsor"
  src = "images/starbucks.jpg"
    [resources.params]
    title = "Starbucks Coffee"
    website = "https://www.starbucks.com"

[[resources]]
  name = "sponsor"
  src = "images/steep_theatre.jpg"
    [resources.params]
    title = "Steep Theatre"
    website = "https://steeptheatre.com/"

[[resources]]
  name = "sponsor"
  src = "images/teatro_vista.jpg"
    [resources.params]
    title = "Teatro Vista"
    website = "https://www.teatrovista.org"

[[resources]]
  name = "sponsor"
  src = "images/the_book_cellar.jpg"
    [resources.params]
    title = "The Book Cellar"
    website = "https://www.bookcellarinc.com"

[[resources]]
  name = "sponsor"
  src = "images/vosges_haut_chocolat.jpg"
    [resources.params]
    title = "Vosges Haut-Chocolat"
    website = "https://www.vosgeschocolate.com"

[[resources]]
  name = "sponsor"
  src = "images/women_and_children_first.jpg"
    [resources.params]
    title = "Women & Children First"
    website = "https://www.womenandchildrenfirst.com"

+++

The Accidental Shakespeare Theatre Company is grateful for the continued
support of great sponsors like these. If you're interested in joining these
sponsors in supporting our work, please
[get in touch with us](/page/contact).
