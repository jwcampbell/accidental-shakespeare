+++
eventtype = "show"
type = "productions" # this helps to identify which layout template to use.
title = "Macbeth"
date = "2011-06-11"
subtitle = "A staged reading"

tickets_available = "no" # If you enter "yes" here, a ticket link will appear!
ticket_call_to_action = "Get Tickets!"
ticket_link = "https://accidentalshakespeare.org"

event_photos = "yes" # Set this to "yes" if we have post-show photos.
  #event_photos should start with the word "perform" (e.g., perform_1.jpg)

playwrights = "William Shakespeare"
runtime = "Aproximately one hour and thirty minutes"
intermission_count = "1"

location = "Titzal Café, Chicago"

[[production]]
name = "Margaretta Sacco"
title = "Director"

[[cast]]
actor = "Robert Oaks"
role = "Macbeth"

[[cast]]
actor = "Laura Shatkus"
role = "Lady Macbeth"

[[cast]]
actor = "Angeli Primlani"
role = "Second Witch, Ross, Siward, Seyton"

[[cast]]
actor = "Jamie Black"
role = "Third Murderer, Duncan, Old Man, Porter, Apparition"

[[cast]]
actor = "Christa Sablic"
role = "First Murderer, Banquo, Donalblain, Son of Macduff, Apparition"

[[cast]]
actor = "Robin Billadeau"
role = "Second Murderer, Lennox, Fleance, Messenger, Apparition"

[[cast]]
actor = "Stacie Swikle"
role = "Third Witch, Macduff, Captain, Gentlewoman"

[[cast]]
actor = "Julia Kessler"
role = "First Witch, Malcolm, Lady Macduff, Doctor"

[[images]]
url = "/assets/images/Macbeth_reading_3witches.JPG"

[[images]]
url = "/assets/images/Macbeth_reading_Oakes_Jaime.JPG"

[[images]]
url = "/assets/images/Macbeth_reading_cast.JPG"

[[images]]
url = "/assets/images/Macbeth_reading_Shatkus.JPG"

[[resources]]
  name = "main"
  src = "main.jpg"

[[resources]]
  name = "perform"
  src = "perform*.jpg"
+++
One of Shakespeare's greatest plays, Macbeth tells the story of the
treacherous rise and bloody fall of the King of Scotland. Informed by a trio
of witches that he is destined to become King, Macbeth and his wife go to
greater and greater lengths to attain and to hold onto this goal.
